package com.formacion.blockBastaGamer.restController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockBastaGamer.dto.StockDtoGet;
import com.formacion.blockBastaGamer.dto.StockDtoPost;
import com.formacion.blockBastaGamer.service.IStockService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class StockRestController {
	
	@Autowired
	private IStockService stockService;
	
	//EndPoint ver lista de Stock
	@GetMapping("/stocks")
	public List<StockDtoGet> verStock(){
		return stockService.findListaStock();
	}
	
	//EndoPoint para buscar un Stock
	@GetMapping("/stocks/{id}")
	public StockDtoGet buscarStock(@PathVariable Long id) {
		return stockService.findStock(id);
	}
	
	//EndPoint añadir Stock
	@PostMapping("/stocks")
	public StockDtoPost nuevoStock(@RequestBody @Valid StockDtoPost stock) {
		return stockService.addStock(stock);
	}
	
	//EndPoint modificar Stock
	@PutMapping("/stocks/{id}")
	public StockDtoPost modificarStock(@PathVariable Long id, @RequestBody @Valid StockDtoPost stock) {
		return stockService.modifyStock(id, stock);
	}
	
	//EndPoint eliminar Stock
	@DeleteMapping("/stocks/{id}")
	public void eliminarStock(@PathVariable Long id) {
		stockService.deleteStock(id);
	}
}
