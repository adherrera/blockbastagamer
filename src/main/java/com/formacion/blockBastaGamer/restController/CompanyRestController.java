package com.formacion.blockBastaGamer.restController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockBastaGamer.dto.CompanyDto;
import com.formacion.blockBastaGamer.service.ICompanyService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class CompanyRestController {
	
	@Autowired
	private ICompanyService companyService;
	
	@GetMapping("/companies")
	public List<CompanyDto> listarCompanies() {
		return companyService.findListaCompanies();
	}
	
	@GetMapping("/companies/{id}")
	public CompanyDto buscarCompany(@PathVariable Long id) {
		return companyService.findCompany(id);
	}
	
	@PostMapping("/companies")
	public CompanyDto nuevaCompany(@RequestBody @Valid CompanyDto company) {
		return companyService.addCompany(company);
	}
	
	@PutMapping("/companies/{cif}")
	public CompanyDto modificarCompany(@PathVariable Long id, @RequestBody @Valid CompanyDto company) {
		return companyService.modifyCompany(id, company);
	}
	
	@DeleteMapping("/companies/{id}")
	public void borrarCompany(@PathVariable Long id) {
		companyService.deleteCompany(id);
	}
}
