package com.formacion.blockBastaGamer.restController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockBastaGamer.dto.ClienteDto;
import com.formacion.blockBastaGamer.service.IClienteService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class ClienteRestController {
	
	@Autowired
	private IClienteService clienteService;
	
	//EndPoint para ver lista clientes
	@GetMapping("/clientes")
	public List<ClienteDto> listarClientes() {
		return clienteService.findListaClientes();
	}
	
	//EndPoint para buscar un Cliente
	@GetMapping("/clientes/{id}")
	public ClienteDto buscarCliente(@PathVariable Long id) {
		return clienteService.findCliente(id);
	}
	
	//EndPoint para registrar nuevo Cliente
	@PostMapping("/clientes")
	public ClienteDto nuevoCliente(@RequestBody @Valid ClienteDto cliente) {
		return clienteService.addCliente(cliente);
	}
	
	//EndPoint para modificar datos del Cliente
	@PutMapping("/clientes/{id}")
	public ClienteDto modificarCliente(@PathVariable Long id, @RequestBody @Valid ClienteDto cliente) {
		return clienteService.modifyCliente(id, cliente);
	}
	
	//EndPoint para eliminar Cliente
	@DeleteMapping("/clientes/{id}")
	public void borrarCliente(@PathVariable Long id) {
		clienteService.deleteCliente(id);
	}
	
}
