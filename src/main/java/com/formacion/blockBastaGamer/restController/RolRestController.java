package com.formacion.blockBastaGamer.restController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockBastaGamer.dto.RolDto;
import com.formacion.blockBastaGamer.service.IRolService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class RolRestController {
	
	@Autowired
	private IRolService rolService;

	@GetMapping("/roles/{id}")
	public RolDto buscarRol(@PathVariable Long id) {
		return rolService.findRol(id);
	}
	
	@GetMapping("/roles")
	public List<RolDto> mostrarRoles() {
		return rolService.findAllRoles();
	}
	
	@PostMapping("/roles")
	public RolDto nuevoRol(@RequestBody @Valid RolDto rolDto) {
		return rolService.addRol(rolDto);
	}
	
	@PutMapping("/roles/{id}")
	public RolDto modificarRol(@PathVariable Long id, @RequestBody @Valid RolDto rolDto) {
		return rolService.modifyRol(id, rolDto);
	}
	
	@DeleteMapping("/roles/{id}")
	public void eliminarRol(@PathVariable Long id) {
		rolService.deleteRol(id);
	}
}
