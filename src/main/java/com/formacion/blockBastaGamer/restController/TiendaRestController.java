package com.formacion.blockBastaGamer.restController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockBastaGamer.dto.TiendaDto;
import com.formacion.blockBastaGamer.service.ITiendaService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class TiendaRestController {

	@Autowired
	private ITiendaService tiendaService;
	
			@GetMapping("/tiendas")
			public List<TiendaDto> listarTiendas() {
				return tiendaService.findListaTiendas();
			}
			
			@GetMapping("/tiendas/{id}")
			public TiendaDto buscarTienda(@PathVariable Long id) {
				return tiendaService.findTienda(id);
			}
			
			@PostMapping("/tiendas")
			public TiendaDto nuevaTienda(@RequestBody @Valid TiendaDto tienda) {
				return tiendaService.addTienda(tienda);
			}
			
			@PutMapping("/tiendas/{id}")
			public TiendaDto modificarTienda(@PathVariable Long id, @RequestBody @Valid TiendaDto tienda) {
				return tiendaService.modifyTienda(id, tienda);
			}
			
			@DeleteMapping("/tiendas/{id}")
			public void borrarTienda(@PathVariable Long id) {
				tiendaService.deleteTienda(id);
			}
}
