package com.formacion.blockBastaGamer.restController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.blockBastaGamer.dto.JuegoDto;
import com.formacion.blockBastaGamer.service.IJuegoService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class JuegoRestController {
	
	@Autowired
	private IJuegoService juegoService;
	
	@GetMapping("/juegos")
	public List<JuegoDto> listarJuegos() {
		return juegoService.findListaJuegos();
	}
	@GetMapping("/juegos/{id}")
	public JuegoDto buscarJuego(@PathVariable Long id) {
		return juegoService.findJuego(id);
	}
	
	@PostMapping("/juegos")
	public JuegoDto nuevoJuego(@RequestBody @Valid JuegoDto juego) {
		return juegoService.addJuego(juego);
	}
	
	@PutMapping("/juegos/{id}")
	public JuegoDto modificarJuego(@PathVariable Long id, @RequestBody @Valid JuegoDto juego) {
		return juegoService.modifyJuego(id, juego);
	}
			
	@DeleteMapping("/juegos/{id}")
	public void borrarJuego(@PathVariable Long id) {
		juegoService.deleteJuego(id);
	}
	
}
