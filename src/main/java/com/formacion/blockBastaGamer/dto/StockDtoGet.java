package com.formacion.blockBastaGamer.dto;

import com.formacion.blockBastaGamer.enums.EnumEstado;

import lombok.Data;

@Data
public class StockDtoGet {
	
	private Long id;
	private EnumEstado estado;
	private Integer numRef;
	private ClienteDto clienteDto;
	private JuegoDto juegoDto;
	private TiendaDto tiendaDto;
	
}
