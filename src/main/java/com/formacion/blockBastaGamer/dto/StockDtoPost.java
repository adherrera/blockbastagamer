package com.formacion.blockBastaGamer.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.formacion.blockBastaGamer.enums.EnumEstado;

import lombok.Data;

@Data
public class StockDtoPost {

	@NotNull
	private EnumEstado estado;
	@NotNull
	private Integer numRef;
	@NotBlank
	private Long idCliente;
	@NotBlank
	private Long idTienda;
	@NotBlank
	private Long idJuego;
	
}
