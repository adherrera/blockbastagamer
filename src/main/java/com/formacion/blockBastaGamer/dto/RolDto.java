package com.formacion.blockBastaGamer.dto;

import java.util.ArrayList;
import java.util.List;

import com.formacion.blockBastaGamer.enums.EnumPrivilegio;
import com.sun.istack.NotNull;

import lombok.Data;

@Data
public class RolDto {

	private Long id;
	@NotNull
	private EnumPrivilegio privilegio;
	
	private List<ClienteDto> listaClientes = new ArrayList<ClienteDto>();
	
}
