package com.formacion.blockBastaGamer.dto;

import lombok.Data;

@Data
public class ErrorResponse {

	/* Lo primero que tenemos que crear es la clase ResponseError, 
	 * desde aquí construimos el mensaje a enseñar al usuario cuando
	 * salte una excepcion. Nótese que hacemos una sobrecarga del 
	 * contructor apra que te muestre un mensaje en caso de que hayamos
	 * definido alguno previamente para una excepcion particular. */
	
	private String exception;
	private String message;
	private String path;
	
	public ErrorResponse(Exception exception, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.message = exception.getMessage();
		this.path = path;
	}
	
	public ErrorResponse(Exception exception, String message, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.message = message;
		this.path = path;
	}
	
}
