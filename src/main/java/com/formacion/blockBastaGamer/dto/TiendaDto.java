package com.formacion.blockBastaGamer.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TiendaDto {
	
	private Long id;
	@NotBlank
	private String nombreTienda;
	@NotBlank
	private String direccion;
	
	private List<StockDtoGet> listaStocks = new ArrayList<StockDtoGet>();
	
}
