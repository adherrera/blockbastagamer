package com.formacion.blockBastaGamer.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.formacion.blockBastaGamer.enums.EnumCategorias;

import lombok.Data;

@Data
public class JuegoDto {
	
	private Long id;
	@NotBlank(message = "El juego debe tener un título.")
	private String titulo;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate fechaLanzamiento;
	@NotNull
	private EnumCategorias categoria;
	@NotNull
	private Integer pegi;
	@NotEmpty
	private List<CompanyDto> listaCompanies = new ArrayList<CompanyDto>();
}
