package com.formacion.blockBastaGamer.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CompanyDto {
	
	private Long id;
	@NotBlank
	private String cif;
	@NotBlank
	private String nombreCompany;
	
	private List<JuegoDto> listaJuegos = new ArrayList<JuegoDto>();
	
}
