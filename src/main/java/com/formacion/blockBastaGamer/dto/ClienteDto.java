package com.formacion.blockBastaGamer.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ClienteDto {
		
	private Long id;
	@NotBlank
	private String nombreCliente;
	@NotBlank
	private String nif;
	@DateTimeFormat(pattern = "yyyy-MM-dd") 
	@NotNull(message = "La fecha no puede estar vacía")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate fechaNacimiento;
	@Email(message = "El patrón del email no es correcto")
	@NotBlank(message = "El email no puede estar vacío")
	private String correo;
	@NotBlank
	private String nombreUsuario;
	@NotBlank
	private String contrasenia;
	@NotEmpty
	private List<RolDto> listaRoles = new ArrayList<RolDto>();

	private List<StockDtoGet> listaStocks = new ArrayList<StockDtoGet>();
}
