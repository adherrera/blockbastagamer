package com.formacion.blockBastaGamer.enums;

public enum EnumCategorias {
	ACTION,
	SHOOTER,
	STRATEGY,
	SIMULATION,
	SPORT,
	RACE,
	ADVENTURE,
	ROL,
	SANDBOX,
	MUSICAL,
	PUZZLE,
	EDUCATION
}
