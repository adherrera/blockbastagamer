package com.formacion.blockBastaGamer.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.formacion.blockBastaGamer.enums.EnumPrivilegio;

import lombok.Data;

@Entity
@Data
@Table(name = "ROL")
public class Rol {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idRol")
	private Long idRol;
	@Column(name = "PRIVILEGIO")
	private EnumPrivilegio privilegio;
	@ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "roles")
	private List<Cliente> clientes = new ArrayList<Cliente>();
}
