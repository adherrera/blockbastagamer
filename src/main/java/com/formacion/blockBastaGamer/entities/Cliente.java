package com.formacion.blockBastaGamer.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CLIENTE")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idCliente")
	private Long idCliente;
	@Column(name= "NOMBRE")
	private String nombreCliente;
	@Column(name= "NIF")
	private String nif;
	@Column(name= "FECHA_NACIMIENTO")
	private LocalDate fechaNacimiento;
	@Column(name = "CORREO")
	private String correo;
	@Column(name = "NOMBRE_USUARIO")
	private String nombreUsuario;
	@Column(name = "PASSWORD")
	private String contrasenia;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
	private List<Stock> stocks = new ArrayList<Stock>();
	@ManyToMany(cascade = CascadeType.PERSIST)
	private List<Rol> roles = new ArrayList<Rol>();

	
}
