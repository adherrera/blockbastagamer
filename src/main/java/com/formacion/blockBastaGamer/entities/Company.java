package com.formacion.blockBastaGamer.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "COMPANY")
@Embeddable
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idCompany")
	private Long idCompany;
	@Column(name = "CIF")
	private String cif;
	@Column(name = "NOMBRE_COMPANIA")
	private String nombreCompany;
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "companias")
	private List<Juego> juegos = new ArrayList<Juego>();
}
