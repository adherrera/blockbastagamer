package com.formacion.blockBastaGamer.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.formacion.blockBastaGamer.enums.EnumCategorias;

import lombok.Data;

@Entity
@Data
@Table(name = "JUEGO")
public class Juego {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idJuego")
	private Long idJuego;
	@Column(name = "TITULO")
	private String titulo;
	@Column(name = "FECHA_LANZAMIENTO")
	private LocalDate fechaLanzamiento;
	@Column(name = "CATEGORIA")
	private EnumCategorias categoria;
	@Column(name = "PEGI")
	private Integer pegi;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
	private List<Stock> stocks = new ArrayList<Stock>();
	@ManyToMany(cascade= CascadeType.PERSIST)
	private List<Company> companias = new ArrayList<Company>();
}
