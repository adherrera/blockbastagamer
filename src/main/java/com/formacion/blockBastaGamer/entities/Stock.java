package com.formacion.blockBastaGamer.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.formacion.blockBastaGamer.enums.EnumEstado;

import lombok.Data;

@Entity
@Data
@Table(name = "STOCK")
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idStock")
	private Long idStock;
	@Column(name = "ESTADO")
	private EnumEstado estado;
	@Column(name = "NUM_REFERENCIA")
	private Integer numRef;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Cliente cliente;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Tienda tienda;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Juego juego;
}
