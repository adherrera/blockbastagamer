package com.formacion.blockBastaGamer.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "TIENDA")
public class Tienda {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idTienda")
	private Long idTienda;
	
	@Column(name = "NOMBRE_TIENDA")
	private String nombreTienda;
	@Column(name = "DIRECCION")
	private String direccion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tienda")
	private List<Stock> stocks;
}
