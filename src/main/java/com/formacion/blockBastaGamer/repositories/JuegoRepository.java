package com.formacion.blockBastaGamer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockBastaGamer.entities.Juego;

@Repository
public interface JuegoRepository extends JpaRepository<Juego, Long>{

	public Juego findByTitulo(String titulo);
	public boolean existsByTitulo(String titulo);

}
