package com.formacion.blockBastaGamer.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockBastaGamer.entities.Cliente;

/* Las clases repository nos permiten ejecutar consultas
 * interactuando con las tablas de la base de datos, en 
 * esta caso, a la entidad Cliente. */

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	/* Los métodos del clienteRepository pueden ampliarse escribiendolos aquí. Además, si utilizas pabalabras clave
	 * te permite realizar consultas automáticamente sin tener que escribir en native SQL. Estas keywords, como el findBy, 
	 * se puede encontrar en internet. La otra manera de hacerlo es escribiendo la consulta a manija con la anotación @Query */
		public Cliente findByNif(String nif);

		public boolean existsByNif(String nif);
		
}
