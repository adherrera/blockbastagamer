package com.formacion.blockBastaGamer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockBastaGamer.entities.Tienda;

@Repository
public interface TiendaRepository extends JpaRepository<Tienda, Long>{

	public Tienda findByNombreTienda(String nombre);

	public boolean existsByNombreTienda(String nombreTienda);

}
