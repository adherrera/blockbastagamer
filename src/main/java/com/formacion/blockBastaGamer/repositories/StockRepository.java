package com.formacion.blockBastaGamer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockBastaGamer.entities.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long>{
	
	Stock findByNumRef(Integer numeroReferencia);

	boolean existsByNumRef(Integer numeroReferencia);

}
