package com.formacion.blockBastaGamer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockBastaGamer.entities.Rol;
import com.formacion.blockBastaGamer.enums.EnumPrivilegio;

@Repository
public interface RolRepository extends JpaRepository<Rol, Long>{

	public Rol findByPrivilegio(EnumPrivilegio privilegio);
	public boolean existsByPrivilegio(EnumPrivilegio privilegio);
	
}
