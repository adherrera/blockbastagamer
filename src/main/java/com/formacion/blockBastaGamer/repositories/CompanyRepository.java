package com.formacion.blockBastaGamer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.blockBastaGamer.entities.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{

	public Company findByCif(String cif);

	public boolean existsByCif(String cif);
	
}
