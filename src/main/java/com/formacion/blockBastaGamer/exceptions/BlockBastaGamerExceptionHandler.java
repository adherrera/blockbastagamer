package com.formacion.blockBastaGamer.exceptions;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.formacion.blockBastaGamer.dto.ErrorResponse;
import com.formacion.blockBastaGamer.exceptions.generic.BadRequestException;
import com.formacion.blockBastaGamer.exceptions.generic.ConflictException;
import com.formacion.blockBastaGamer.exceptions.generic.ForbiddenException;
import com.formacion.blockBastaGamer.exceptions.generic.InternalServerErrorException;
import com.formacion.blockBastaGamer.exceptions.generic.NoContentException;
import com.formacion.blockBastaGamer.exceptions.generic.NotFoundException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class BlockBastaGamerExceptionHandler extends ResponseEntityExceptionHandler {
	
	/* Esta clase es la que estará interceptando lo que llega para saber si el objeto
	 * es una excepcion suya. El @Order se emplea para darle preferencia en el caso de
	 * que tengas otro exceptionHandler en el proyecto. @ControllerAdvice da lugar a 
	 * que entienda que es este tipo de controlador. La idea es que cada bloque de código
	 * escrito aquí represente a una excepción a controlar. */
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundException.class})
	@ResponseBody
	public ErrorResponse notFoundException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	//Creamos otro bloque de código
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({BadRequestException.class})
	@ResponseBody
	public ErrorResponse badRequestException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({NoContentException.class})
	@ResponseBody
	public ErrorResponse noContentException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({
		InternalServerErrorException.class,
		java.lang.NullPointerException.class
	})
	@ResponseBody
	public ErrorResponse internalServerErrorException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler({ForbiddenException.class})
	@ResponseBody
	public ErrorResponse forbiddenException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler({ConflictException.class})
	@ResponseBody
	public ErrorResponse conflictException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
		return new ResponseEntity<>(new ErrorResponse(ex, errorMessages.toString(), request.getContextPath()) , HttpStatus.BAD_REQUEST);
	}
}
