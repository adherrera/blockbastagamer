package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.ConflictException;

public class StockConflictException extends ConflictException {
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Stock Conflict";
	
	public StockConflictException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
