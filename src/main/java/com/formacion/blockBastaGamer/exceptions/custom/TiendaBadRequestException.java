package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.BadRequestException;

public class TiendaBadRequestException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Tienda Bad Request";
	
	public TiendaBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
