package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.ForbiddenException;

public class StockForbiddenException extends ForbiddenException{

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Stock Forbidden Exception";
	
	public StockForbiddenException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
