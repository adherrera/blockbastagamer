package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.BadRequestException;

public class RolBadRequestException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Rol Bad Request";
	
	public RolBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
