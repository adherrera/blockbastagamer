package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.ConflictException;

public class CompanyConflictException extends ConflictException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Company Conflict";
	
	public CompanyConflictException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
