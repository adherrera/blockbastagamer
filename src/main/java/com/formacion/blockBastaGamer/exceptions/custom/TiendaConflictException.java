package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.ConflictException;

public class TiendaConflictException extends ConflictException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Tienda Conflict";
	
	public TiendaConflictException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
