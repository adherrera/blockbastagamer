package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.BadRequestException;

public class CompanyBadRequestException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Company Bad Request";
	
	public CompanyBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
	
}
