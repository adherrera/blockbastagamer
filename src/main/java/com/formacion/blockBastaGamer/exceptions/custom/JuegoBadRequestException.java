package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.BadRequestException;

public class JuegoBadRequestException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Juego Bad Request";
	
	public JuegoBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
