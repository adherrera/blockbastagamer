package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.BadRequestException;

public class StockBadRequestException extends BadRequestException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Stock Bad Request";
	
	public StockBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
