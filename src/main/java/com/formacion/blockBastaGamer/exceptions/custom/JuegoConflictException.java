package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.ConflictException;

public class JuegoConflictException extends ConflictException {
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Juego Conflict";
	
	public JuegoConflictException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
