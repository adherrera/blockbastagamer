package com.formacion.blockBastaGamer.exceptions.custom;

import com.formacion.blockBastaGamer.exceptions.generic.ConflictException;

public class RolConflictException extends ConflictException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Rol Conflict";
	
	public RolConflictException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
