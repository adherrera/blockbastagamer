package com.formacion.blockBastaGamer.service;

import java.util.List;

import com.formacion.blockBastaGamer.dto.TiendaDto;

public interface ITiendaService {

	public TiendaDto addTienda(TiendaDto tiendaDto);

	public TiendaDto modifyTienda(Long id, TiendaDto tienda);

	public List<TiendaDto> findListaTiendas();

	public TiendaDto findTienda(Long id);

	public void deleteTienda(Long id);
}
