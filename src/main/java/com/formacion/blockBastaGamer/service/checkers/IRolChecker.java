package com.formacion.blockBastaGamer.service.checkers;

import com.formacion.blockBastaGamer.enums.EnumPrivilegio;

public interface IRolChecker {

	public void siRolNoExisteLanzaBadRequest(Long id);

	public void siPrivilegioExisteLanzaConflict(EnumPrivilegio privilegio);
	
}
