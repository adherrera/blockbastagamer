package com.formacion.blockBastaGamer.service.checkers.checkersImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.exceptions.custom.TiendaBadRequestException;
import com.formacion.blockBastaGamer.exceptions.custom.TiendaConflictException;
import com.formacion.blockBastaGamer.repositories.TiendaRepository;
import com.formacion.blockBastaGamer.restController.TiendaRestController;
import com.formacion.blockBastaGamer.service.checkers.ITiendaChecker;

@Service
public class TiendaCheckerImpl implements ITiendaChecker {

	@Autowired
	private TiendaRepository tiendaRepository;
	
	private Logger loggerTienda = LoggerFactory.getLogger(TiendaRestController.class);
	
	@Override
	public void siTiendaExisteLanzaConflict(String nombreTienda) {
		if (tiendaRepository.existsByNombreTienda(nombreTienda)) {
			loggerTienda.error("Se produjo un error al introducir una nueva tienda porque su nombre ya estaba en la BBDD.");
			throw new TiendaConflictException("Alguna tienda con ese nombre ya está en la base de datos.");
		}
	}
	
	@Override
	public void siTiendaNoExisteLanzaBadRequest(Long id) {
		if (!tiendaRepository.existsById(id)) {
			loggerTienda.error("Se produjo un error al buscar una tienda que no existía en la BBDD.");
			throw new TiendaBadRequestException("Esa tienda no existe en la base de datos.");
		}
	}
}
