package com.formacion.blockBastaGamer.service.checkers;

public interface ITiendaChecker {

	public void siTiendaExisteLanzaConflict(String nombreTienda);
	
	public void siTiendaNoExisteLanzaBadRequest(Long id);
}
