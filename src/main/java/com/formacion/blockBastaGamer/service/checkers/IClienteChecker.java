package com.formacion.blockBastaGamer.service.checkers;

import com.formacion.blockBastaGamer.dto.ClienteDto;

public interface IClienteChecker {
	
	public void validarCliente(ClienteDto clienteDto);
	
	public void siClienteNoExisteLanzaBadRequest(Long id);

}