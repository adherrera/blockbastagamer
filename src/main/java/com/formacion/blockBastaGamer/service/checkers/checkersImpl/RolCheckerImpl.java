package com.formacion.blockBastaGamer.service.checkers.checkersImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.enums.EnumPrivilegio;
import com.formacion.blockBastaGamer.exceptions.custom.RolBadRequestException;
import com.formacion.blockBastaGamer.exceptions.custom.RolConflictException;
import com.formacion.blockBastaGamer.repositories.RolRepository;
import com.formacion.blockBastaGamer.restController.RolRestController;
import com.formacion.blockBastaGamer.service.checkers.IRolChecker;

@Service
public class RolCheckerImpl implements IRolChecker {

	@Autowired
	private RolRepository rolRepository;
	
	private Logger loggerRol = LoggerFactory.getLogger(RolRestController.class);

	@Override
	public void siRolNoExisteLanzaBadRequest(Long id) {
		if (!rolRepository.existsById(id)) {
			loggerRol.error("Se produjo un error al buscar un rol que no existía en la BBDD.");
			throw new RolBadRequestException("Ese rol no existe en la base de datos.");
		}
	}

	@Override
	public void siPrivilegioExisteLanzaConflict(EnumPrivilegio privilegio) {
		if (rolRepository.existsByPrivilegio(privilegio)) {
			loggerRol.error("Se produjo un error al introducir un nuevo rol porque su privilegio ya estaba en la BBDD.");
			throw new RolConflictException("Ya hay un rol con ese privilegio en la base de datos.");
		}
	}
	
}
