package com.formacion.blockBastaGamer.service.checkers;

public interface ICompanyChecker {

	public void siCompanyExisteLanzaConflict(String cif);
	
	public void siCompanyNoExisteLanzaBadRequest(Long id);	

}
