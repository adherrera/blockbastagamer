package com.formacion.blockBastaGamer.service.checkers;

import java.util.List;

import com.formacion.blockBastaGamer.dto.StockDtoPost;

public interface IStockChecker {

	public void validarStock(StockDtoPost stockDto);
	
	public void validarListaStock(List<StockDtoPost> listaStocksDto);
	
	public void siStockNoExisteLanzaBadRequest(Long id);

}
