package com.formacion.blockBastaGamer.service.checkers.checkersImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.exceptions.custom.CompanyBadRequestException;
import com.formacion.blockBastaGamer.exceptions.custom.CompanyConflictException;
import com.formacion.blockBastaGamer.repositories.CompanyRepository;
import com.formacion.blockBastaGamer.restController.CompanyRestController;
import com.formacion.blockBastaGamer.service.checkers.ICompanyChecker;

@Service
public class CompanyCheckerImpl implements ICompanyChecker {

	@Autowired
	private CompanyRepository companyRepository;
	
	private Logger loggerCompany = LoggerFactory.getLogger(CompanyRestController.class);
	
	@Override
	public void siCompanyExisteLanzaConflict(String cif) {
		if (companyRepository.existsByCif(cif)) {
			loggerCompany.error("Se produjo un error al introducir una nueva company porque ya estaba en la BBDD con ese cif.");
			throw new CompanyConflictException("Alguna company con ese cif ya está en la base de datos.");
		}
	}
	
	@Override
	public void siCompanyNoExisteLanzaBadRequest(Long id) {
		if (!companyRepository.existsById(id)) {
			loggerCompany.error("Se produjo un error al buscar una company que no existía en la BBDD con ese cif.");
			throw new CompanyBadRequestException("La company con ese cif no existe en la base de datos.");
		}
	}

}
