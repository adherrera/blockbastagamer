package com.formacion.blockBastaGamer.service.checkers;

import com.formacion.blockBastaGamer.dto.JuegoDto;

public interface IJuegoChecker {
	
	public void validarJuego(JuegoDto juegoDto);

	public void siJuegoNoExisteLanzaBadRequest(Long id);

}
