package com.formacion.blockBastaGamer.service.checkers.checkersImpl;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.StockDtoPost;
import com.formacion.blockBastaGamer.entities.Stock;
import com.formacion.blockBastaGamer.enums.EnumEstado;
import com.formacion.blockBastaGamer.exceptions.custom.StockBadRequestException;
import com.formacion.blockBastaGamer.exceptions.custom.StockConflictException;
import com.formacion.blockBastaGamer.exceptions.custom.StockForbiddenException;
import com.formacion.blockBastaGamer.repositories.ClienteRepository;
import com.formacion.blockBastaGamer.repositories.JuegoRepository;
import com.formacion.blockBastaGamer.repositories.StockRepository;
import com.formacion.blockBastaGamer.repositories.TiendaRepository;
import com.formacion.blockBastaGamer.restController.StockRestController;
import com.formacion.blockBastaGamer.service.checkers.IStockChecker;

@Service
public class StockCheckerImpl implements IStockChecker {

	@Autowired
	private StockRepository stockRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private TiendaRepository tiendaRepository;
	
	private Logger loggerStock = LoggerFactory.getLogger(StockRestController.class);
	
	private void siStockExisteLanzaConflict(Integer numeroReferencia) {
		if (stockRepository.existsByNumRef(numeroReferencia)) {
			loggerStock.error("Se produjo un error al introducir un stock cuyo número de referencia ya existe en la BBDD.");
			throw new StockConflictException("El numero de referencia de ese stock ya está en la BBDD.");
		}
	}
	
	private void siClienteNoExisteLanzaBadRequest(Long id) {
		if (!clienteRepository.existsById(id)) {
			loggerStock.error("Se produjo un error al introducir un stock porque el cliente no existe en a BBDD con ese nif.");
			throw new StockBadRequestException("El cliente que quiere asociar al stock no se encuentra en la BBDD por ese nif.");
		}
	}
	
	private void siTiendaNoExisteLanzaBadRequest(Long id) {
		if (!tiendaRepository.existsById(id)) {
			loggerStock.error("Se produjo un error al introducir un stock porque la tienda no existe en a BBDD por ese nombre.");
			throw new StockBadRequestException("La tienda que quiere asociar al stock no se encuentra en la BBDD por ese nombre.");
		}
	}
	
	private void siJuegoNoExisteLanzaBadRequest(Long id) {
		if (!juegoRepository.existsById(id)) {
			loggerStock.error("Se produjo un error al introducir un stock porque el juego no existe en a BBDD con ese título.");
			throw new StockBadRequestException("El juego que quiere asociar al stock no se encuentra en la BBDD por ese título.");
		}
	}
	
	private void compararEdadConPegi(StockDtoPost stockDto) {
		Integer pegiJuego = juegoRepository.findById(stockDto.getIdJuego()).get().getPegi();
		Integer edadCliente = Period.between(clienteRepository.findById(stockDto.getIdCliente()).get().getFechaNacimiento(), LocalDate.now()).getYears();
		if (edadCliente <= pegiJuego) {
			throw new StockForbiddenException("El cliente no tiene edad suficiente para adquirir ese juego.");
		}
	}
	
	private void comprobarSiExisteAlquilerEnStockDelCliente(StockDtoPost stockDto) {
		if (stockDto.getEstado().equals(EnumEstado.ALQUILADO)) {
			for (Stock stock : clienteRepository.findById(stockDto.getIdCliente()).get().getStocks()) {
				if (stock.getEstado().equals(EnumEstado.ALQUILADO)) {
					throw new StockForbiddenException("El cliente ya tiene un juego alquilado y no puede alquilar otro.");
				}
			}			
		}
	}
	
	@Override
	public void validarStock(StockDtoPost stockDto) {
		this.siStockExisteLanzaConflict(stockDto.getNumRef());
		this.siClienteNoExisteLanzaBadRequest(stockDto.getIdCliente());
		this.siTiendaNoExisteLanzaBadRequest(stockDto.getIdTienda());
		this.siJuegoNoExisteLanzaBadRequest(stockDto.getIdJuego());
		this.comprobarSiExisteAlquilerEnStockDelCliente(stockDto);
		this.compararEdadConPegi(stockDto);
	}
	
	@Override
	public void validarListaStock(List<StockDtoPost> listaStocksDto) {
		for (StockDtoPost stockDto : listaStocksDto) {
			this.validarStock(stockDto);
		}
	}
	
	@Override
	public void siStockNoExisteLanzaBadRequest(Long id) {
		if (!stockRepository.existsById(id)) {
			loggerStock.error("Se produjo un error al buscar un stock que no existe en la BBDD.");
			throw new StockBadRequestException("El stock que busca no se encuentra en la BBDD.");
		}
	}

}
