package com.formacion.blockBastaGamer.service.checkers.checkersImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.ClienteDto;
import com.formacion.blockBastaGamer.dto.RolDto;
import com.formacion.blockBastaGamer.exceptions.custom.ClienteBadRequestException;
import com.formacion.blockBastaGamer.exceptions.custom.ClienteConflictException;
import com.formacion.blockBastaGamer.repositories.ClienteRepository;
import com.formacion.blockBastaGamer.repositories.RolRepository;
import com.formacion.blockBastaGamer.restController.ClienteRestController;
import com.formacion.blockBastaGamer.service.checkers.IClienteChecker;

@Service
public class ClienteCheckerImpl implements IClienteChecker {

	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private RolRepository rolRepository;
	
	private Logger loggerCliente = LoggerFactory.getLogger(ClienteRestController.class);
	
	private void siNifExisteLanzaConflict(String nif) {
		if (clienteRepository.existsByNif(nif)) {
			loggerCliente.error("Se produjo un error al introducir un nuevo cliente porque su nif ya estaba en la BBDD.");
			throw new ClienteConflictException("Ya hay un cliente con ese nif en la base de datos.");
		}
	}
	
	private void siPrivilegioNoExistenLanzaBadRequest(List<RolDto> listaRolesDto) {
		for (RolDto rolDto : listaRolesDto) {
			if (!rolRepository.existsByPrivilegio(rolDto.getPrivilegio())) {
				loggerCliente.error("Se produjo un error al introducir un cliente porque alguno de sus privilegios no se encuentra en la BBDD.");
				throw new ClienteBadRequestException("Algún privilegio asociado a un cliente no existe en la base de datos.");
			}
		}
	}
	
	@Override
	public void validarCliente(ClienteDto clienteDto) {
		this.siNifExisteLanzaConflict(clienteDto.getNif());
		this.siPrivilegioNoExistenLanzaBadRequest(clienteDto.getListaRoles());
	}
	
	@Override
	public void siClienteNoExisteLanzaBadRequest(Long id) {
		if (!clienteRepository.existsById(id)) {
			loggerCliente.error("Se produjo un error al buscar un cliente que no existía en la BBDD.");
			throw new ClienteBadRequestException("El cliente con ese nif no existe en la base de datos.");
		}
	}

}
