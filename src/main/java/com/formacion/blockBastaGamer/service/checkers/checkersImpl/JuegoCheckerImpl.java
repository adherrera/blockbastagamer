package com.formacion.blockBastaGamer.service.checkers.checkersImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.CompanyDto;
import com.formacion.blockBastaGamer.dto.JuegoDto;
import com.formacion.blockBastaGamer.exceptions.custom.JuegoBadRequestException;
import com.formacion.blockBastaGamer.exceptions.custom.JuegoConflictException;
import com.formacion.blockBastaGamer.repositories.CompanyRepository;
import com.formacion.blockBastaGamer.repositories.JuegoRepository;
import com.formacion.blockBastaGamer.restController.JuegoRestController;
import com.formacion.blockBastaGamer.service.checkers.IJuegoChecker;

@Service
public class JuegoCheckerImpl implements IJuegoChecker {

	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private CompanyRepository companyRepository;

	private Logger loggerJuego = LoggerFactory.getLogger(JuegoRestController.class);
	
	private void siJuegoExisteLanzaConflict(String titulo) {
		if (juegoRepository.existsByTitulo(titulo)) {
			loggerJuego.error("Se produjo un error al introducir un nuevo juego porque ya existe uno con ese título en la BBDD.");
			throw new JuegoConflictException("Algún juego con ese título ya está en la base de datos.");
		}
	}
	
	private void siAlgunaCompanyDelJuegoNoExisteLanzaBadRequest(List<CompanyDto> listaCompaniesDto) {
		for (CompanyDto companyDto : listaCompaniesDto) {
			if (!companyRepository.existsByCif(companyDto.getCif())) {
				loggerJuego.error("Se produjo un error al asociar una lista de companies a un juego porque alguna no existía en la BBDD por el cif indicadp.");
				throw new JuegoBadRequestException("Alguna de las companies de la lista asociada al juego no existe en la base de datos por el cif indicado.");
			}			
		}
	}
	
	@Override
	public void validarJuego(JuegoDto juegoDto) {
		this.siJuegoExisteLanzaConflict(juegoDto.getTitulo());
		this.siAlgunaCompanyDelJuegoNoExisteLanzaBadRequest(juegoDto.getListaCompanies());
	}

	@Override
	public void siJuegoNoExisteLanzaBadRequest(Long id) {
		if (!juegoRepository.existsById(id)) {
			loggerJuego.error("Se produjo un error al buscar un juego que no existía en la BBDD por ese título.");
			throw new JuegoBadRequestException("El juego no existe en la base de datos.");
		}
	}
	
}
