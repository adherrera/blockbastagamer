package com.formacion.blockBastaGamer.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockBastaGamer.dto.ClienteDto;
import com.formacion.blockBastaGamer.entities.Cliente;
import com.formacion.blockBastaGamer.repositories.ClienteRepository;
import com.formacion.blockBastaGamer.service.IClienteService;
import com.formacion.blockBastaGamer.service.checkers.IClienteChecker;
import com.formacion.blockBastaGamer.service.converters.IClienteConverter;

@Service
public class ClienteServiceImpl implements IClienteService {
	
	//Atributos
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private IClienteConverter clienteConverter;
	@Autowired
	private IClienteChecker clienteChecker;

	//Métodos
	/* Ahora los métodos transforman el objeto que les llega del Dto
	 * en un objeto Entidad para la base de datos */

	@Override
	@Transactional
	public ClienteDto addCliente(ClienteDto clienteDto) {
		clienteChecker.validarCliente(clienteDto);
		clienteRepository.save(clienteConverter.convertDtoToEntity(clienteDto));
		return clienteDto;
	}

	@Override
	@Transactional
	public ClienteDto modifyCliente(Long id, ClienteDto clienteDto) {
		clienteChecker.siClienteNoExisteLanzaBadRequest(id);
		Cliente clienteToModify = clienteRepository.findById(id).get();
		clienteToModify.setCorreo(clienteDto.getCorreo());
		clienteToModify.setFechaNacimiento(clienteDto.getFechaNacimiento());
		clienteToModify.setNif(clienteDto.getNif());
		clienteToModify.setNombreCliente(clienteDto.getNombreCliente());
		clienteRepository.save(clienteToModify);
		return clienteDto;
	}

	@Override
	@Transactional
	public void deleteCliente(Long id) {
		clienteChecker.siClienteNoExisteLanzaBadRequest(id);
		clienteRepository.delete(clienteRepository.findById(id).get());		
	}

	@Override
	@Transactional(readOnly = true)
	public ClienteDto findCliente(Long id) {
		clienteChecker.siClienteNoExisteLanzaBadRequest(id);
		return clienteConverter.convertEntityToDto(clienteRepository.findById(id).get(), true, true);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ClienteDto> findListaClientes() {
		return clienteConverter.convertEntityListToDtoList(clienteRepository.findAll(), true, true);
	}

}
