package com.formacion.blockBastaGamer.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockBastaGamer.dto.TiendaDto;
import com.formacion.blockBastaGamer.entities.Tienda;
import com.formacion.blockBastaGamer.repositories.TiendaRepository;
import com.formacion.blockBastaGamer.service.ITiendaService;
import com.formacion.blockBastaGamer.service.checkers.ITiendaChecker;
import com.formacion.blockBastaGamer.service.converters.ITiendaConverter;

@Service
public class TiendaServiceImpl implements ITiendaService {

	@Autowired
	private TiendaRepository tiendaRepository;
	@Autowired
	private ITiendaConverter tiendaConverter;
	@Autowired
	private ITiendaChecker tiendaChecker;

	
	@Override
	@Transactional
	public TiendaDto addTienda(TiendaDto tiendaDto) {
		tiendaChecker.siTiendaExisteLanzaConflict(tiendaDto.getNombreTienda());
		tiendaRepository.save(tiendaConverter.convertDtoToEntity(tiendaDto));
		return tiendaDto;
	}

	@Override
	@Transactional
	public TiendaDto modifyTienda(Long id, TiendaDto tiendaDto) {
		tiendaChecker.siTiendaNoExisteLanzaBadRequest(id);
		Tienda tiendaToModify = tiendaRepository.findById(id).get();
		tiendaToModify.setDireccion(tiendaDto.getDireccion());
		tiendaToModify.setNombreTienda(tiendaDto.getNombreTienda());
		tiendaRepository.save(tiendaToModify);
		return tiendaDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<TiendaDto> findListaTiendas() {
		return tiendaConverter.convertEntityListToDtoList(tiendaRepository.findAll(), true);
	}

	@Override
	@Transactional(readOnly = true)
	public TiendaDto findTienda(Long id) {
		tiendaChecker.siTiendaNoExisteLanzaBadRequest(id);
		return tiendaConverter.convertEntityToDto(tiendaRepository.findById(id).get(), true);
	}

	@Override
	@Transactional
	public void deleteTienda(Long id) {
		tiendaChecker.siTiendaNoExisteLanzaBadRequest(id);
		tiendaRepository.delete(tiendaRepository.findById(id).get());
	}
	
}
