package com.formacion.blockBastaGamer.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockBastaGamer.dto.JuegoDto;
import com.formacion.blockBastaGamer.entities.Juego;
import com.formacion.blockBastaGamer.repositories.JuegoRepository;
import com.formacion.blockBastaGamer.service.IJuegoService;
import com.formacion.blockBastaGamer.service.checkers.IJuegoChecker;
import com.formacion.blockBastaGamer.service.converters.IJuegoConverter;

@Service
public class JuegoServiceImpl implements IJuegoService{
	
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private IJuegoConverter juegoConverter;
	@Autowired
	private IJuegoChecker juegoChecker;
	
	@Override
	@Transactional
	public JuegoDto addJuego(JuegoDto juegoDto) {
		juegoChecker.validarJuego(juegoDto);
		juegoRepository.save(juegoConverter.convertDtoToEntity(juegoDto));
		return juegoDto;
	}

	@Override
	@Transactional
	public JuegoDto modifyJuego(Long id, JuegoDto juegoDto) {
		juegoChecker.siJuegoNoExisteLanzaBadRequest(id);
		Juego juegoToModify = juegoRepository.findById(id).get();
		juegoToModify.setCategoria(juegoDto.getCategoria());
		juegoToModify.setFechaLanzamiento(juegoDto.getFechaLanzamiento());
		juegoToModify.setPegi(juegoDto.getPegi());
		juegoToModify.setTitulo(juegoDto.getTitulo());
		juegoRepository.save(juegoToModify);
		return juegoDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<JuegoDto> findListaJuegos() {
		return juegoConverter.convertEntityListToDtoList(juegoRepository.findAll(), true);
	}

	@Override
	@Transactional(readOnly = true)
	public JuegoDto findJuego(Long id) {
		juegoChecker.siJuegoNoExisteLanzaBadRequest(id);
		return juegoConverter.convertEntityToDto(juegoRepository.findById(id).get(), true);
	}

	@Override
	@Transactional
	public void deleteJuego(Long id) {
		juegoChecker.siJuegoNoExisteLanzaBadRequest(id);
		juegoRepository.delete(juegoRepository.findById(id).get());
	}

}
