package com.formacion.blockBastaGamer.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockBastaGamer.dto.CompanyDto;
import com.formacion.blockBastaGamer.entities.Company;
import com.formacion.blockBastaGamer.repositories.CompanyRepository;
import com.formacion.blockBastaGamer.service.ICompanyService;
import com.formacion.blockBastaGamer.service.checkers.ICompanyChecker;
import com.formacion.blockBastaGamer.service.converters.ICompanyConverter;

@Service
public class CompanyServiceImpl implements ICompanyService {

	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private ICompanyConverter companyConverter;
	@Autowired
	private ICompanyChecker companyChecker;
	
	@Override
	@Transactional
	public CompanyDto addCompany(CompanyDto companyDto) {
		companyChecker.siCompanyExisteLanzaConflict(companyDto.getCif());
		companyRepository.save(companyConverter.convertDtoToEntity(companyDto));
		return companyDto;
	}

	@Override
	@Transactional
	public CompanyDto modifyCompany(Long id, CompanyDto companyDto) {
		companyChecker.siCompanyNoExisteLanzaBadRequest(id);
		Company companyToModify = companyRepository.findById(id).get();
		companyToModify.setCif(companyDto.getCif());
		companyToModify.setNombreCompany(companyDto.getNombreCompany());
		companyRepository.save(companyToModify);
		return companyDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CompanyDto> findListaCompanies() {
		return companyConverter.convertEntityListToDtoList(companyRepository.findAll(), true);
	}

	@Override
	@Transactional(readOnly = true)
	public CompanyDto findCompany(Long id) {
		companyChecker.siCompanyNoExisteLanzaBadRequest(id);
		return companyConverter.convertEntityToDto(companyRepository.findById(id).get(), true);
	}

	@Override
	@Transactional
	public void deleteCompany(Long id) {
		companyChecker.siCompanyNoExisteLanzaBadRequest(id);
		companyRepository.delete(companyRepository.findById(id).get());
	}

}
