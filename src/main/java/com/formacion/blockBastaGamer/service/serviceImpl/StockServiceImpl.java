package com.formacion.blockBastaGamer.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockBastaGamer.dto.StockDtoGet;
import com.formacion.blockBastaGamer.dto.StockDtoPost;
import com.formacion.blockBastaGamer.entities.Stock;
import com.formacion.blockBastaGamer.repositories.StockRepository;
import com.formacion.blockBastaGamer.service.IStockService;
import com.formacion.blockBastaGamer.service.checkers.IStockChecker;
import com.formacion.blockBastaGamer.service.converters.IStockConverter;

@Service
public class StockServiceImpl implements IStockService {

	@Autowired
	private StockRepository stockRepository;
	@Autowired
	private IStockConverter stockConverter;
	@Autowired
	private IStockChecker stockChecker;
	
	@Override
	@Transactional(readOnly = true)
	public List<StockDtoGet> findListaStock() {
		return stockConverter.convertEntityListToDtoList(stockRepository.findAll(), true, true, true);
	}

	@Override
	@Transactional(readOnly = true)
	public StockDtoGet findStock(Long id) {
		stockChecker.siStockNoExisteLanzaBadRequest(id);
		return stockConverter.convertEntityToDto(stockRepository.findById(id).get(), true, true, true);
	}

	@Override
	@Transactional
	public StockDtoPost addStock(StockDtoPost stockDto) {
		stockChecker.validarStock(stockDto);
		stockRepository.save(stockConverter.convertDtoToEntity(stockDto));
		return stockDto;
	}
	
	@Override
	@Transactional
	public StockDtoPost modifyStock(Long id, StockDtoPost stockDto) {
		stockChecker.siStockNoExisteLanzaBadRequest(id);
		stockChecker.validarStock(stockDto);
		Stock stockToModify = stockRepository.findById(id).get();
		stockToModify.setEstado(stockDto.getEstado());
		stockToModify.setNumRef(stockDto.getNumRef());
		stockRepository.save(stockToModify);
		return stockDto;
	}

	@Override
	@Transactional
	public void deleteStock(Long id) {
		stockChecker.siStockNoExisteLanzaBadRequest(id);
		stockRepository.delete(stockRepository.findById(id).get());
	}

}
