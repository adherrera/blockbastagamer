package com.formacion.blockBastaGamer.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.blockBastaGamer.dto.RolDto;
import com.formacion.blockBastaGamer.entities.Rol;
import com.formacion.blockBastaGamer.repositories.RolRepository;
import com.formacion.blockBastaGamer.service.IRolService;
import com.formacion.blockBastaGamer.service.checkers.IRolChecker;
import com.formacion.blockBastaGamer.service.converters.IRolConverter;

@Service
public class RolServiceImpl implements IRolService {

	@Autowired
	private RolRepository rolRepository;
	@Autowired
	private IRolConverter rolConverter;
	@Autowired
	private IRolChecker rolChecker;
	
	@Override
	@Transactional(readOnly = true)
	public RolDto findRol(Long id) {
		rolChecker.siRolNoExisteLanzaBadRequest(id);
		return rolConverter.convertEntityToDto(rolRepository.findById(id).get(), true);
	}

	@Override
	@Transactional(readOnly = true)
	public List<RolDto> findAllRoles() {
		return rolConverter.convertEntityListToDtoList(rolRepository.findAll(), true);
	}

	@Override
	@Transactional
	public RolDto addRol(RolDto rolDto) {
		rolChecker.siPrivilegioExisteLanzaConflict(rolDto.getPrivilegio());
		rolRepository.save(rolConverter.convertDtoToEntity(rolDto));
		return rolDto;
	}

	@Override
	@Transactional
	public RolDto modifyRol(Long id, RolDto rolDto) {
		rolChecker.siRolNoExisteLanzaBadRequest(id);
		Rol rolToModify = rolRepository.findById(id).get();
		rolToModify.setPrivilegio(rolDto.getPrivilegio());
		return rolDto;
	}

	@Override
	@Transactional
	public void deleteRol(Long id) {
		rolChecker.siRolNoExisteLanzaBadRequest(id);
		rolRepository.delete(rolRepository.findById(id).get());
	}

}
