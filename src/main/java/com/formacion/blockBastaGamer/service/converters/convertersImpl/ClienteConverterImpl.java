package com.formacion.blockBastaGamer.service.converters.convertersImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.ClienteDto;
import com.formacion.blockBastaGamer.entities.Cliente;
import com.formacion.blockBastaGamer.service.converters.IClienteConverter;

@Service
public class ClienteConverterImpl implements IClienteConverter {
	
	@Autowired
	private StockConverterImpl stockConverter;
	@Autowired
	private RolConverterImpl rolConverter;

	@Override
	public Cliente convertDtoToEntity(ClienteDto clienteDto) {
		Cliente clienteEntidad = new Cliente();
		clienteEntidad.setCorreo(clienteDto.getCorreo());
		clienteEntidad.setFechaNacimiento(clienteDto.getFechaNacimiento());
		clienteEntidad.setNif(clienteDto.getNif());
		clienteEntidad.setNombreCliente(clienteDto.getNombreCliente());
		clienteEntidad.setNombreUsuario(clienteDto.getNombreUsuario());
		clienteEntidad.setContrasenia(clienteDto.getContrasenia());
		clienteEntidad.setRoles(rolConverter.findRolesInDB(clienteDto.getListaRoles()));
		return clienteEntidad;
	}
	
	@Override
	public ClienteDto convertEntityToDto(Cliente clienteEntidad, boolean addRoles, boolean addStocks) {
		ClienteDto clienteDto = new ClienteDto();
		clienteDto.setId(clienteEntidad.getIdCliente());
		clienteDto.setCorreo(clienteEntidad.getCorreo());
		clienteDto.setFechaNacimiento(clienteEntidad.getFechaNacimiento());
		clienteDto.setNif(clienteEntidad.getNif());
		clienteDto.setNombreCliente(clienteEntidad.getNombreCliente());
		clienteDto.setNombreUsuario(clienteEntidad.getNombreUsuario());
		clienteDto.setContrasenia("**********");
		if (addRoles) {
			clienteDto.setListaRoles(rolConverter.convertEntityListToDtoList(clienteEntidad.getRoles(), false));
		}
		if (addStocks) {
			clienteDto.setListaStocks(stockConverter.convertEntityListToDtoList(clienteEntidad.getStocks(), false, true, true));
		}
		return clienteDto;
	}
	
	@Override
	public List<ClienteDto> convertEntityListToDtoList(List<Cliente> listaClientesEntidad, boolean addRoles, boolean addStocks) {
		List<ClienteDto> listaClientesDto = new ArrayList<ClienteDto>();
		for (Cliente clienteEntidad : listaClientesEntidad) {
			listaClientesDto.add(this.convertEntityToDto(clienteEntidad, addRoles, addStocks));
		}
		return listaClientesDto;
	}

}
