package com.formacion.blockBastaGamer.service.converters.convertersImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.RolDto;
import com.formacion.blockBastaGamer.entities.Rol;
import com.formacion.blockBastaGamer.repositories.RolRepository;
import com.formacion.blockBastaGamer.service.converters.IClienteConverter;
import com.formacion.blockBastaGamer.service.converters.IRolConverter;

@Service
public class RolConverterImpl implements IRolConverter {
	
	@Autowired
	IClienteConverter clienteConverter;
	@Autowired
	private RolRepository rolRepository;
	
	@Override
	public Rol convertDtoToEntity(RolDto rolDto) {
		Rol rolEntidad = new Rol();
		rolEntidad.setPrivilegio(rolDto.getPrivilegio());
		return rolEntidad;
	}

	@Override
	public RolDto convertEntityToDto(Rol rolEntidad, boolean addClientes) {
		RolDto rolDto = new RolDto();
		rolDto.setId(rolEntidad.getIdRol());
		rolDto.setPrivilegio(rolEntidad.getPrivilegio());
		if (addClientes) {
			rolDto.setListaClientes(clienteConverter.convertEntityListToDtoList(rolEntidad.getClientes(), false, false));
		}
		return rolDto;
	}

	@Override
	public List<RolDto> convertEntityListToDtoList(List<Rol> listaRolesEntidad, boolean addClientes) {
		List<RolDto> listaRolesDto = new ArrayList<RolDto>();
		for (Rol rolEntidad : listaRolesEntidad) {
			listaRolesDto.add(this.convertEntityToDto(rolEntidad, addClientes));
		}
		return listaRolesDto;
	}
	
	@Override
	public List<Rol> convertDtoListToEntityList(List<RolDto> listaRolesDto) {
		List<Rol> listaRolesEntidad = new ArrayList<Rol>();
		for (RolDto rolDto : listaRolesDto) {
			listaRolesEntidad.add(this.convertDtoToEntity(rolDto));
		}
		return listaRolesEntidad;
	}

	@Override
	public List<Rol> findRolesInDB(List<RolDto> listaRoles) {
		List<Rol> listaRolesEntidad = new ArrayList<Rol>();
		for (RolDto rolDto : listaRoles) {
			listaRolesEntidad.add(rolRepository.findByPrivilegio(rolDto.getPrivilegio()));
		}
		return listaRolesEntidad;
	}
	
	
}
