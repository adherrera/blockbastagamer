package com.formacion.blockBastaGamer.service.converters;

import java.util.List;

import com.formacion.blockBastaGamer.dto.ClienteDto;
import com.formacion.blockBastaGamer.entities.Cliente;

public interface IClienteConverter {

	public Cliente convertDtoToEntity(ClienteDto clienteDto);

	public ClienteDto convertEntityToDto(Cliente cliente, boolean addRoles, boolean addStocks);

	public List<ClienteDto> convertEntityListToDtoList(List<Cliente> listaClientesEntidad, boolean addRoles, boolean addStocks);

}
