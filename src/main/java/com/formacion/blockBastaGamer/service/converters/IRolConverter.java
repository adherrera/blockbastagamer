package com.formacion.blockBastaGamer.service.converters;

import java.util.List;

import com.formacion.blockBastaGamer.dto.RolDto;
import com.formacion.blockBastaGamer.entities.Rol;

public interface IRolConverter {

	public Rol convertDtoToEntity(RolDto rolDto);

	public RolDto convertEntityToDto(Rol rolEntidad, boolean addClientes);

	public List<RolDto> convertEntityListToDtoList(List<Rol> listaRolesEntidad, boolean addClientes);

	public List<Rol> convertDtoListToEntityList(List<RolDto> listaRolesDto);

	public List<Rol> findRolesInDB(List<RolDto> listaRoles);

}
