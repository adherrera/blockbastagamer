package com.formacion.blockBastaGamer.service.converters;

import java.util.List;

import com.formacion.blockBastaGamer.dto.StockDtoGet;
import com.formacion.blockBastaGamer.dto.StockDtoPost;
import com.formacion.blockBastaGamer.entities.Stock;

public interface IStockConverter {

	public Stock convertDtoToEntity(StockDtoPost stockDto);

	public StockDtoGet convertEntityToDto(Stock stockEntidad, boolean addCliente, boolean addJuego, boolean addTienda);

	public List<StockDtoGet> convertEntityListToDtoList(List<Stock> listaStockEntidad, boolean addCliente, boolean addJuego, boolean addTienda);

}
