package com.formacion.blockBastaGamer.service.converters.convertersImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.TiendaDto;
import com.formacion.blockBastaGamer.entities.Tienda;
import com.formacion.blockBastaGamer.repositories.TiendaRepository;
import com.formacion.blockBastaGamer.service.converters.IStockConverter;
import com.formacion.blockBastaGamer.service.converters.ITiendaConverter;

@Service
public class TiendaConverterImpl implements ITiendaConverter {
	
	@Autowired
	private TiendaRepository tiendaRepository;
	@Autowired
	private IStockConverter stockConverter;

	@Override
	public Tienda convertDtoToEntity(TiendaDto tiendaDto) {
		Tienda tiendaEntidad = new Tienda();
		tiendaEntidad.setNombreTienda(tiendaDto.getNombreTienda());
		tiendaEntidad.setDireccion(tiendaDto.getDireccion());
		return tiendaEntidad;
	}
	
	@Override
	public TiendaDto convertEntityToDto(Tienda tiendaEntidad, boolean addStocks) {
		TiendaDto tiendaDto = new TiendaDto();
		tiendaDto.setId(tiendaEntidad.getIdTienda());
		tiendaDto.setNombreTienda(tiendaEntidad.getNombreTienda());
		tiendaDto.setDireccion(tiendaEntidad.getDireccion());
		if (addStocks) {
			tiendaDto.setListaStocks(stockConverter.convertEntityListToDtoList(tiendaEntidad.getStocks(), true, true, false));
		}
		return tiendaDto;
	}
	
	@Override
	public List<TiendaDto> convertEntityListToDtoList(List<Tienda> listaTiendasEntidad, boolean addStocks) {
		List<TiendaDto> listaTiendasDto = new ArrayList<TiendaDto>();
		for (Tienda tiendaEntidad : listaTiendasEntidad) {
			listaTiendasDto.add(this.convertEntityToDto(tiendaEntidad, addStocks));
		}
		return listaTiendasDto;
	}

	@Override
	public Optional<Tienda> findTiendaInDB(Long idTienda) {
		return tiendaRepository.findById(idTienda);
	}

}
