package com.formacion.blockBastaGamer.service.converters;

import java.util.List;

import com.formacion.blockBastaGamer.dto.JuegoDto;
import com.formacion.blockBastaGamer.entities.Juego;

public interface IJuegoConverter {

	public Juego convertDtoToEntity(JuegoDto juegoDto);

	public JuegoDto convertEntityToDto(Juego juegoEntidad, boolean addCompanies);

	public List<JuegoDto> convertEntityListToDtoList(List<Juego> listaJuegosEntidad, boolean addCompanies);

}
