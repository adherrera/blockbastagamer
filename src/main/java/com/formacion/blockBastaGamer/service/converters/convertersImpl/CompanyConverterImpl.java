package com.formacion.blockBastaGamer.service.converters.convertersImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.CompanyDto;
import com.formacion.blockBastaGamer.entities.Company;
import com.formacion.blockBastaGamer.repositories.CompanyRepository;
import com.formacion.blockBastaGamer.service.converters.ICompanyConverter;

@Service
public class CompanyConverterImpl implements ICompanyConverter {
	
	@Autowired
	private JuegoConverterImpl juegoConverter;
	@Autowired
	private CompanyRepository companyRepository;

	@Override
	public Company convertDtoToEntity(CompanyDto companyDto) {
		Company companyEntidad = new Company();
		companyEntidad.setCif(companyDto.getCif());
		companyEntidad.setNombreCompany(companyDto.getNombreCompany());
		return companyEntidad;
	}
	
	@Override
	public CompanyDto convertEntityToDto(Company companyEntidad, boolean addJuegos) {
		CompanyDto companyDto = new CompanyDto();
		companyDto.setId(companyEntidad.getIdCompany());
		companyDto.setCif(companyEntidad.getCif());
		companyDto.setNombreCompany(companyEntidad.getNombreCompany());
		if (addJuegos) {
			companyDto.setListaJuegos(juegoConverter.convertEntityListToDtoList(companyEntidad.getJuegos(), false));
		}
		return companyDto;
	}
	
	@Override
	public List<CompanyDto> convertEntityListToDtoList(List<Company> listaCompanyEntidad, boolean addJuegos) {
		List<CompanyDto> listaCompanyDto = new ArrayList<CompanyDto>();
		for (Company companyEntidad : listaCompanyEntidad) {
			listaCompanyDto.add(this.convertEntityToDto(companyEntidad, addJuegos));
		}
		return listaCompanyDto;
	}
	
	@Override
	public List<Company> findCompaniesInDB(List<CompanyDto> listaCompaniesDto) {
		List<Company> listaCompaniesEntidad = new ArrayList<Company>();
		for (CompanyDto companyDto : listaCompaniesDto) {
			listaCompaniesEntidad.add(companyRepository.findByCif(companyDto.getCif()));
		}
		return listaCompaniesEntidad;
	}

}
