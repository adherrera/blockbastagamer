package com.formacion.blockBastaGamer.service.converters;

import java.util.List;
import java.util.Optional;

import com.formacion.blockBastaGamer.dto.TiendaDto;
import com.formacion.blockBastaGamer.entities.Tienda;

public interface ITiendaConverter {

	public Tienda convertDtoToEntity(TiendaDto tiendaDto);

	public TiendaDto convertEntityToDto(Tienda tiendaEntidad, boolean addStocks);

	public List<TiendaDto> convertEntityListToDtoList(List<Tienda> listaTiendasEntidad, boolean addStocks);

	public Optional<Tienda> findTiendaInDB(Long idTienda);

}
