package com.formacion.blockBastaGamer.service.converters.convertersImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.JuegoDto;
import com.formacion.blockBastaGamer.entities.Juego;
import com.formacion.blockBastaGamer.service.converters.IJuegoConverter;

@Service
public class JuegoConverterImpl implements IJuegoConverter {
	
	@Autowired
	private CompanyConverterImpl companyConverter;

	@Override
	public Juego convertDtoToEntity(JuegoDto juegoDto) {
		Juego juegoEntidad = new Juego();
		juegoEntidad.setCategoria(juegoDto.getCategoria());
		juegoEntidad.setFechaLanzamiento(juegoDto.getFechaLanzamiento());
		juegoEntidad.setPegi(juegoDto.getPegi());
		juegoEntidad.setTitulo(juegoDto.getTitulo());
		juegoEntidad.setCompanias(companyConverter.findCompaniesInDB((juegoDto.getListaCompanies())));
		return juegoEntidad;
	}
	
	@Override
	public JuegoDto convertEntityToDto(Juego juegoEntidad, boolean addCompanies) {
		JuegoDto juegoDto = new JuegoDto();
		juegoDto.setId(juegoEntidad.getIdJuego());
		juegoDto.setCategoria(juegoEntidad.getCategoria());
		juegoDto.setFechaLanzamiento(juegoEntidad.getFechaLanzamiento());
		juegoDto.setPegi(juegoEntidad.getPegi());
		juegoDto.setTitulo(juegoEntidad.getTitulo());
		if (addCompanies) {
			juegoDto.setListaCompanies(companyConverter.convertEntityListToDtoList(juegoEntidad.getCompanias(), false));
		}
		return juegoDto;
	}
	
	@Override
	public List<JuegoDto> convertEntityListToDtoList(List<Juego> listaJuegosEntidad, boolean addCompanies) {
		List<JuegoDto> listaJuegosDto = new ArrayList<JuegoDto>();
		for (Juego juegoEntidad : listaJuegosEntidad) {
			listaJuegosDto.add(this.convertEntityToDto(juegoEntidad, addCompanies));
		}
		return listaJuegosDto;
	}
	
}
