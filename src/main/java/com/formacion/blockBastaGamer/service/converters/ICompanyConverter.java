package com.formacion.blockBastaGamer.service.converters;

import java.util.List;

import com.formacion.blockBastaGamer.dto.CompanyDto;
import com.formacion.blockBastaGamer.entities.Company;

public interface ICompanyConverter {

	public Company convertDtoToEntity(CompanyDto companyDto);

	public CompanyDto convertEntityToDto(Company companyEntidad, boolean addJuegos);

	public List<CompanyDto> convertEntityListToDtoList(List<Company> listaCompanyEntidad, boolean addJuegos);

	public List<Company> findCompaniesInDB(List<CompanyDto> listaCompaniesDto);

}
