package com.formacion.blockBastaGamer.service.converters.convertersImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.blockBastaGamer.dto.StockDtoGet;
import com.formacion.blockBastaGamer.dto.StockDtoPost;
import com.formacion.blockBastaGamer.entities.Stock;
import com.formacion.blockBastaGamer.repositories.ClienteRepository;
import com.formacion.blockBastaGamer.repositories.JuegoRepository;
import com.formacion.blockBastaGamer.repositories.TiendaRepository;
import com.formacion.blockBastaGamer.service.converters.IClienteConverter;
import com.formacion.blockBastaGamer.service.converters.IJuegoConverter;
import com.formacion.blockBastaGamer.service.converters.IStockConverter;
import com.formacion.blockBastaGamer.service.converters.ITiendaConverter;

@Service
public class StockConverterImpl implements IStockConverter {
	
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private TiendaRepository tiendaRepository;
	
	@Autowired
	private IClienteConverter clienteConverter;
	@Autowired
	private IJuegoConverter juegoConverter;
	@Autowired
	private ITiendaConverter tiendaConverter;
	
	@Override
	public Stock convertDtoToEntity(StockDtoPost stockDto) {
		Stock stockEntity = new Stock();
		stockEntity.setEstado(stockDto.getEstado());
		stockEntity.setNumRef(stockDto.getNumRef());
		stockEntity.setCliente(clienteRepository.findById(stockDto.getIdCliente()).get());
		stockEntity.setJuego(juegoRepository.findById(stockDto.getIdJuego()).get());
		stockEntity.setTienda(tiendaRepository.findById(stockDto.getIdTienda()).get());
		return stockEntity;
	}
	
	@Override
	public StockDtoGet convertEntityToDto(Stock stockEntidad, boolean addCliente, boolean addJuego, boolean addTienda) {
		StockDtoGet stockDto = new StockDtoGet();
		stockDto.setId(stockEntidad.getIdStock());
		stockDto.setEstado(stockEntidad.getEstado());
		stockDto.setNumRef(stockEntidad.getNumRef());
		if (addCliente) {
			stockDto.setClienteDto(clienteConverter.convertEntityToDto(stockEntidad.getCliente(), true, false));
		}
		if (addJuego) {
			stockDto.setJuegoDto(juegoConverter.convertEntityToDto(stockEntidad.getJuego(), true));
		}
		if (addTienda) {
			stockDto.setTiendaDto(tiendaConverter.convertEntityToDto(stockEntidad.getTienda(), false));
		}
		return stockDto;
	}
	
	@Override
	public List<StockDtoGet> convertEntityListToDtoList(List<Stock> listaStockEntidad, boolean addCliente, boolean addJuego, boolean addTienda) {
		List<StockDtoGet> listaStockDto = new ArrayList<StockDtoGet>();
		for (Stock stockEntidad : listaStockEntidad) {
			listaStockDto.add(this.convertEntityToDto(stockEntidad, addCliente, addJuego, addTienda));
		}
		return listaStockDto;
	}

}
