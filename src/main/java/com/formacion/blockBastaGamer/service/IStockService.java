package com.formacion.blockBastaGamer.service;

import java.util.List;

import com.formacion.blockBastaGamer.dto.StockDtoGet;
import com.formacion.blockBastaGamer.dto.StockDtoPost;

public interface IStockService {

	public StockDtoPost addStock(StockDtoPost stock);
	
	public List<StockDtoGet> findListaStock();

	public StockDtoGet findStock(Long id);

	public StockDtoPost modifyStock(Long id, StockDtoPost stock);

	public void deleteStock(Long id);

}
