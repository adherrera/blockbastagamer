package com.formacion.blockBastaGamer.service;

import java.util.List;

import com.formacion.blockBastaGamer.dto.ClienteDto;

public interface IClienteService {
	
	public ClienteDto addCliente(ClienteDto clienteDto);
	
	public ClienteDto modifyCliente(Long id, ClienteDto clienteDto);
	
	public void deleteCliente(Long id);
	
	public ClienteDto findCliente(Long id);
	
	public List<ClienteDto> findListaClientes();

}
