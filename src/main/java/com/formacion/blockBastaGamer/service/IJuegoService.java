package com.formacion.blockBastaGamer.service;

import java.util.List;

import com.formacion.blockBastaGamer.dto.JuegoDto;

public interface IJuegoService {

	public JuegoDto addJuego(JuegoDto juego);

	public JuegoDto modifyJuego(Long id, JuegoDto juego);

	public List<JuegoDto> findListaJuegos();

	public JuegoDto findJuego(Long id);

	public void deleteJuego(Long id);

}
