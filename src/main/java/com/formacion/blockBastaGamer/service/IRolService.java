package com.formacion.blockBastaGamer.service;

import java.util.List;

import com.formacion.blockBastaGamer.dto.RolDto;

public interface IRolService {

	public RolDto findRol(Long id);

	public List<RolDto> findAllRoles();

	public RolDto addRol(RolDto rolDto);

	public RolDto modifyRol(Long id, RolDto rolDto);

	public void deleteRol(Long id);

}
