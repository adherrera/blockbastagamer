package com.formacion.blockBastaGamer.service;

import java.util.List;

import com.formacion.blockBastaGamer.dto.CompanyDto;

public interface ICompanyService {

	public CompanyDto addCompany(CompanyDto company);

	public CompanyDto modifyCompany(Long id, CompanyDto company);

	public List<CompanyDto> findListaCompanies();

	public CompanyDto findCompany(Long id);

	public void deleteCompany(Long id);

}
