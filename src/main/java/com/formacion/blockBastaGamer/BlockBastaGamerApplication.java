package com.formacion.blockBastaGamer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlockBastaGamerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlockBastaGamerApplication.class, args);
		System.out.println("Ready to go!");
	}

}
